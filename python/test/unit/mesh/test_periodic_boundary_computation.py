"""Unit tests for PeriodicBoundaryComputation"""

# Copyright (C) 2013 Mikael Mortensen
#
# This file is part of DOLFIN.
#
# DOLFIN is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DOLFIN is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DOLFIN. If not, see <http://www.gnu.org/licenses/>.
#
# First added:  2013-04-12
# Last changed: 2014-05-30

import pytest
import numpy as np
from dolfin import *

from dolfin_utils.test import *


@fixture
def periodic_boundary():
    class PeriodicBoundary(SubDomain):
        def inside(self, x, on_boundary):
            return x[0] < DOLFIN_EPS

        def map(self, x, y):
            y[0] = x[0] - 1.0
            y[1] = x[1]

    return PeriodicBoundary()

@fixture
def periodic_boundary_master():
    class PeriodicBoundaryMaster(SubDomain):
        def inside(self, x, on_boundary):
            return abs(x[0]) < DOLFIN_EPS

    return PeriodicBoundaryMaster()

@fixture
def periodic_boundary_slave():
    class PeriodicBoundarySlave(SubDomain):
        def inside(self, x, on_boundary):
            return abs(x[0]-1.0) < DOLFIN_EPS

    return PeriodicBoundarySlave()

@fixture
def periodic_boundary_map():
    class PeriodicBoundaryMap(SubDomain):
        def map(self, x, y):
            y[0] = x[0] - 1.0
            y[1] = x[1]

    return PeriodicBoundaryMap()

@fixture
def mesh():
    return UnitSquareMesh(4, 4)


@skip_in_parallel
def test_ComputePeriodicPairs(periodic_boundary, mesh):

    # Verify that correct number of periodic pairs are computed
    vertices = PeriodicBoundaryComputation.compute_periodic_pairs(mesh, periodic_boundary, 0)
    edges = PeriodicBoundaryComputation.compute_periodic_pairs(mesh, periodic_boundary, 1)
    assert len(vertices) == 5
    assert len(edges) == 4

@skip_in_parallel
def test_ComputePeriodicPairs_meshfunction(periodic_boundary_map, 
                                           periodic_boundary_master, periodic_boundary_slave, 
                                           mesh):

    # Verify that correct number of periodic pairs are computed
    facet_markers = MeshFunction("size_t", mesh, 1)
    facet_markers.set_all(0)
    periodic_boundary_master.mark(facet_markers, 1)
    master_domains = np.array([1], dtype=np.uintp)
    periodic_boundary_slave.mark(facet_markers, 2)
    slave_domains = np.array([2], dtype=np.uintp)

    vertices = PeriodicBoundaryComputation.compute_periodic_pairs(mesh, periodic_boundary_map, 
                                          facet_markers, master_domains, slave_domains, 0)
    edges    = PeriodicBoundaryComputation.compute_periodic_pairs(mesh, periodic_boundary_map, 
                                          facet_markers, master_domains, slave_domains, 1)
    assert len(vertices) == 5
    assert len(edges) == 4


@skip_in_parallel
def test_MastersSlaves(periodic_boundary, mesh):

    # Verify that correct number of masters and slaves are marked
    mf = PeriodicBoundaryComputation.masters_slaves(mesh, periodic_boundary, 0)
    assert len(np.where(mf.array() == 1)[0]) == 5
    assert len(np.where(mf.array() == 2)[0]) == 5

    mf = PeriodicBoundaryComputation.masters_slaves(mesh, periodic_boundary, 1)
    assert len(np.where(mf.array() == 1)[0]) == 4
    assert len(np.where(mf.array() == 2)[0]) == 4

@skip_in_parallel
def test_MastersSlaves_meshfunction(periodic_boundary_map, 
                                    periodic_boundary_master, periodic_boundary_slave, 
                                    mesh):

    # Verify that correct number of masters and slaves are marked
    facet_markers = MeshFunction("size_t", mesh, 1)
    facet_markers.set_all(0)
    periodic_boundary_master.mark(facet_markers, 1)
    master_domains = np.array([1], dtype=np.uintp)
    periodic_boundary_slave.mark(facet_markers, 2)
    slave_domains = np.array([2], dtype=np.uintp)

    mf = PeriodicBoundaryComputation.masters_slaves(mesh, periodic_boundary_map, facet_markers, \
                            master_domains, slave_domains, 0)
    assert len(np.where(mf.array() == 1)[0]) == 5
    assert len(np.where(mf.array() == 2)[0]) == 5

    mf = PeriodicBoundaryComputation.masters_slaves(mesh, periodic_boundary_map, facet_markers, \
                            master_domains, slave_domains, 1)
    assert len(np.where(mf.array() == 1)[0]) == 4
    assert len(np.where(mf.array() == 2)[0]) == 4

